document.addEventListener( 'DOMContentLoaded', function() { 
    const 
        domCmdMenu = document.getElementById('cmd-menu'),
        domCadOuv = document.getElementById('cadOuv'),
        domCadFer = document.getElementById('cadFerme'),
        domNavMenu = document.getElementById('nav_menu'),
        domFormContact = document.getElementById('formContact'),
        domContact = document.getElementById('btn-contact');
    
    domCmdMenu.addEventListener( 'click', function() {

        let isMenuHidden = domNavMenu.classList.contains( 'hidden' );
       

        if( isMenuHidden ) {
            domNavMenu.classList.remove( 'hidden' );
            domCadFer.classList.add( 'hidden' );
            domCadOuv.classList.remove( 'hidden' );

        }
        else {
            domNavMenu.classList.add( 'hidden' );
            domCmdMenu.classList.remove( 'close' );
            domCadOuv.classList.add( 'hidden' );
            domCadFer.classList.remove( 'hidden' );
        }
    });

    domContact.addEventListener( 'click', function() {

        let isFormHidden = domFormContact.classList.contains( 'hidden' );
       

        if( isFormHidden ) {
            domFormContact.classList.remove( 'hidden' );
        }
        else {
            domFormContact.classList.add( 'hidden' );
        }
    });

});
